<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>

</head>

<body><em></em>



	<?php include('chat.php'); ?>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

		<?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2>La Página que buscas no existe, Te dejamos nuestros datos de contacto.</h2>



				</div>

			</div>



			<!-- contact box -->

			<div class="contact-box">

				<div class="container">

					<div class="row">



						<div class="col-md-6" align="center">



							<div class="container">

								<div class="col-md-12" >

									<a href="https://api.whatsapp.com/send?phone=524434716989&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">

										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">

										</i> <font size="6"> WhatsApp</font> 

									</button>

								</a>

							</div>

						</div>



						<br>



						<div class="container">

							<div class="col-md-12" >

								<?php include('form.php'); ?>

							</div>

						</div>

					</div>





					<div class="col-md-3">

						<div class="contact-information">

							<h3>Información de Contacto</h3>

							<ul class="contact-information-list">

								<li><span><i class="fa fa-home"></i>Av. Doctor Río de la Loza #45</span> <span>Col. Doctores</span> <span>Delegación Cuauhtémoc, México, Distrito Federal.</span></li>

								<li><span><i class="fa fa-phone"></i>(55) 5588 5688</span></li>

								<li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 117</strong></span><br>



									<i class="fa fa-phone"></i><span>Postventa <strong>Ext. 124</strong></span><br>                                    

									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 113</strong></span><br>

									<i class="fa fa-phone"></i><span>Ventas <strong>Ext. 106</strong></span><br>

									<i class="fa fa-phone"></i><span>Recepción <strong>Ext. 110</strong></span><br>                                    

									<i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 120</strong></span><br>

									<i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 217</strong></span><br>

									<i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 105</strong></span><br>  

								</li>

								<h3>Whatsapp</h3>

								<li>

									<span>

										<i class="fa fa-whatsapp"></i>

										<strong> Ventas y Postventa <br> 

											<a href="https://api.whatsapp.com/send?phone=524434716989&text=Hola,%20Quiero%20más%20información!" title="Ventas">

												4434716989

											</a>  |  

											<a href="https://api.whatsapp.com/send?phone=525513537530 &text=Hola,%20Quiero%20más%20información!" title="Post Venta">

												5513537530 

											</a>

										</strong>

									</span>

								</li>

								<li>

									<span>

										<i class="fa fa-whatsapp"></i>

										<strong>   Citas   y   Servicio <br> 

											<a href="https://api.whatsapp.com/send?phone=525537311738&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">

												5537311738

											</a> |  

											<a href="https://api.whatsapp.com/send?phone=525554187976&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">

												5554187976

											</a>

										</strong>

									</span>

								</li>

								<li>

									<span>

										<i class="fa fa-whatsapp"></i>

										<strong>   Calidad <br> 

											<a href="https://api.whatsapp.com/send?phone=525538997738 &text=Hola,%20Quiero%20más%20información!" title="Calidad">

												5538997738 

											</a> 

										</strong>

									</span>

								</li>



							</ul>

						</div>

					</div>



					<div class="col-md-3">

						<div class="contact-information">

							<h3>Horario de Atención</h3>

							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Monarca, D.F.</strong>; te escuchamos y atendemos de manera personalizada. </p>

							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 8:00 p.m.</p>

							<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 5:00 p.m.</p>

							<p class="work-time"><span>Domingo</span> : 11:00 a.m. - 5:00 p.m.</p>

						</div>

					</div>







				</div>

			</div>

		</div>



	</div> 



	<br><br><br><br><br><br><br>



	<?php include('contenido/footer.php'); ?>

</div> 			



</body>

</html>